package br.com.senac.exercicio10;

import java.util.ArrayList;
import java.util.List;

public class Boletim {

    public double notaDosAlunos(List<Aluno> lista) {

        double quant = 0;

        for (Aluno a : lista) {
            double media = (a.getNota1() + a.getNota2() / 2);

            if (media >= 7) {
                quant = quant + 1;
            }
        }
        return quant;
    }

}
