package br.com.senac.exercicio10;

import java.util.ArrayList;
import java.util.List;
import org.junit.Test;
import static org.junit.Assert.*;

public class Exercicio10Test {

    public Exercicio10Test() {
    }

    @Test
    public void notaMaior7() {

        List<Aluno> lista = new ArrayList<>();
        Aluno messi = new Aluno(10, 10, "Messi");
        Aluno cr7 = new Aluno(7, 7, "Cr7");
        Aluno neymar = new Aluno(2, 2, "Neymar");

        lista.add(messi);
        lista.add(cr7);
        lista.add(neymar);

        Boletim boletim = new Boletim();

        double resultado = boletim.notaDosAlunos(lista);
        
        assertEquals(2, resultado, 0.01);
        
        
    }

}
